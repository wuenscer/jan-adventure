#include <stdio.h>

struct GameEntity
{
    UINT8 width;
    UINT8 height;
    UINT8 x;
    UINT8 y;
    unsigned char spriteData[];
};
