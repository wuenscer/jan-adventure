#include <gb/gb.h>
#include "sprites/FaceSprites.c"
#include "sprites/backgrounds.c"
#include "sprites/backgroundmap.c"
#include <stdio.h>
/*
Start off on a back alley and make your way up to the Oberstudiendirektor!

Gameboy screen:
160x144
*/

UINT8 playerSpriteID = 0;
UINT8 jumpHeight = 10;
UINT8 jumpCurrentHeight = 0;
INT8 gravity = -2;
INT8 jumpVelocity;
BYTE isJumping = 0;
UINT8 playerLocation[] = {68,118}; //x,y


void performantDelay(UINT8 numLoops){
    UINT8 i;
    for (i = 0; i < numLoops; i++)
    {
        wait_vbl_done();
    }
}

UINT8 isSurfaceHit(){
    return 0;
}

void playSoundJump(){
    NR10_REG = 0x16;
    NR11_REG = 0x40;
    NR12_REG = 0x73;
    NR13_REG = 0x00;
    NR14_REG = 0xC3;
}

void jump(UINT8 spriteID, UINT8 spriteLocation[2]){
    if(isJumping==0){
        playSoundJump();
        jumpVelocity=10;
        isJumping=1;
    }
    jumpVelocity = jumpVelocity + gravity;
    spriteLocation[1] = spriteLocation[1] - jumpVelocity;
    jumpCurrentHeight = jumpCurrentHeight + jumpVelocity;
    if(jumpCurrentHeight<=0){
        isJumping=0;
    }
}

int init(){
    /* Enable Sound */
    // these registers must be in this specific order!
    // see https://github.com/bwhitman/pushpin/blob/master/src/gbsound.txt
    NR52_REG = 0x80; // is 1000 0000 in binary and turns on sound
    NR50_REG = 0x77; // sets the volume for both left and right channel just set to max 0x77
    NR51_REG = 0xFF; // is 1111 1111 in binary, select which chanels we want to use in this case all of them. One bit for the L one bit for the R of all four channels

    /* Set background */
    set_bkg_data(0,7,backgrounds);
    set_bkg_tiles(0,0,40,18,backgroundmap);

    /* set sprites */
    set_sprite_data(0,2,SimpleFace);
    set_sprite_tile(playerSpriteID,0);
    move_sprite(playerSpriteID,playerLocation[0],playerLocation[1]);

    /* start the show */
    SHOW_BKG;
    SHOW_SPRITES;
    DISPLAY_ON;
    return 0;
}

int main()
{
    int initialization = init();
    UINT8 tileIndex=0;
    //printf("Jan's Adventure\nStart off on a\nback alley and make\nyour way up to the\n\nOberstudiendirektor!");
    
    while (1)
    {
        if(tileIndex == 0){
            tileIndex = 1;
        } else {
            tileIndex = 0;
        }
        set_sprite_tile(playerSpriteID,tileIndex);
        
        if((joypad() & J_A) || (joypad() & J_UP) || isJumping){
            jump(playerSpriteID, playerLocation);
        }
        if(joypad() & J_LEFT){
            //scroll_sprite(0,-10,0);
            scroll_bkg(-10,0);
        }
        if(joypad() & J_RIGHT){
            //scroll_sprite(0,-10,0);
            scroll_bkg(10,0);
        }

        move_sprite(playerSpriteID,playerLocation[0],playerLocation[1]);
        delay(100);
    }
}
